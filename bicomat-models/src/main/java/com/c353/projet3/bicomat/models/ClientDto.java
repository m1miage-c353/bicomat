package com.c353.projet3.bicomat.models;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ClientDto {
    private String id;

    private String numeroCompte;

    private String nom;

    private String prenom;

    private String email;

    private String typeClient;

    private CompteBancaireDto compteBancaireDto;
}
