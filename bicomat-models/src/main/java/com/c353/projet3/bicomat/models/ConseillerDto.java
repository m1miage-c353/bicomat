package com.c353.projet3.bicomat.models;

public class ConseillerDto {
    private String id;

    private String nom;

    private String prenom;

    private String login;

    private String motDePasse;

    private ClientInterneDto clientInterneDto;
}
