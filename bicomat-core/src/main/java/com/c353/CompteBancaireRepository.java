package com.c353;

import com.c353.projet3.bicomat.models.CompteBancaireDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompteBancaireRepository extends JpaRepository<CompteBancaireDto,String> {
}
