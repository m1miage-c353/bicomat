package com.c353.projet3.bicomat.core.controllers;

import com.c353.projet3.bicomat.core.services.GenericServiceInt;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public abstract class GenericRessource<E, ID, DTO>  {
    protected abstract GenericServiceInt getService();

    @PostMapping("/save")
    public ResponseEntity save(@RequestBody @Valid DTO dto) {
        return ResponseEntity.ok(this.getService().save(dto));
    }

    @PostMapping("/update/{id}")
    public ResponseEntity update(@PathVariable ID id, @RequestBody @Valid DTO dto) {
        if( getService().exists(id) ) {
            ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Element non trouvé");
        }
        return ResponseEntity.ok(this.getService().save(dto));
    }

    @GetMapping("/view/{id}")
    public ResponseEntity view(@PathVariable ID id) {
        return getService().exists(id) ? ResponseEntity.ok(this.getService().findById(id))
                : ResponseEntity.status(HttpStatus.NOT_FOUND).body("Element non trouvé");
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable ID id) {
        return getService().exists(id) ? ResponseEntity.ok(this.getService().findById(id))
                : ResponseEntity.status(HttpStatus.NOT_FOUND).body("Element non trouvé");
    }

    @GetMapping("/list")
    public ResponseEntity<List> list() {
        return ResponseEntity.ok(this.getService().findAll());
    }
}
