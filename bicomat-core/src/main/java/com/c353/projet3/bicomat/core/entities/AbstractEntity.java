package com.c353.projet3.bicomat.core.entities;

import jakarta.persistence.Column;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.MappedSuperclass;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.io.Serializable;
import java.time.Instant;

@Data //annotation Lombock pour générer getters, setters, hashcode et tostring
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class) //pour gestion d'audit par hibernate
public class AbstractEntity implements Serializable {
    @CreatedDate
    @Column(name="creationdate")
    private Instant creationDate;

    @LastModifiedDate
    @Column(name="lastmodifieddate")
    private Instant lastModifiedDate;
}
