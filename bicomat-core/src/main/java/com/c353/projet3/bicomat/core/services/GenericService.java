package com.c353.projet3.bicomat.core.services;

import com.c353.projet3.bicomat.core.entities.AbstractEntity;
import jakarta.persistence.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public abstract class GenericService<E extends AbstractEntity, ID, DTO> implements
        GenericServiceInt<E, ID, DTO>{

    protected abstract JpaRepository<E, ID> getRepository();

    public abstract DTO fromEntity(E e);
    public abstract E toEntity(DTO dto);

    public DTO fromEntity(ID id){
        return fromEntity(find(id));
    }

    public DTO save(DTO dto) {
        return fromEntity(getRepository().save(toEntity(dto)));
    }

    @Override
    public boolean exists(ID id) {
        return getRepository().existsById(id);
    }

    @Override
    public E find(ID id) {
        return getRepository().findById(id).orElseThrow(() ->
                new EntityNotFoundException(String.format("Aucune donnee d'identifiant %s n'a été trouvé", id))
        );
    }

    @Override
    public boolean delete(ID id) {
        if (exists(id)) {
            try {
                getRepository().deleteById(id);
                return true;
            } catch (Exception e) {
                log.error(e.getLocalizedMessage(), e);
            }
        }
        return false;
    }

    @Override
    public DTO findById(ID id) {
        return fromEntity(find(id));
    }

    @Override
    public List<DTO> findAll() {
        return getRepository().findAll().stream().map(this::fromEntity).collect(Collectors.toList());
    }
}
