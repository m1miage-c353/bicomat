package com.c353.projet3.bicomat.core.repository;

import com.c353.projet3.bicomat.core.entities.Banque;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BanqueRepository extends JpaRepository<Banque,String> {
}
