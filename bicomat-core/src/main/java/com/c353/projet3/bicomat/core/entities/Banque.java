package com.c353.projet3.bicomat.core.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 *
 * @author AKOESSO Akoélé
 *
 */
@Entity
@Table(name = "banque")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Banque extends AbstractEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    private String nom;

    private String adresse;
}
