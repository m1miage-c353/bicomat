package com.c353.projet3.bicomat.core.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "conseillerlogin")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ConseillerLogin {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    @Column
    private String login;

    @Column
    private String motDePasse;

    @ManyToOne
    @JoinColumn( name = "id_conseiller" )
    private Conseiller conseiller;
}
