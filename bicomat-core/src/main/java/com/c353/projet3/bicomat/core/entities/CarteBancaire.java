package com.c353.projet3.bicomat.core.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@Entity
@Table(name = "cartebancaire")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CarteBancaire extends AbstractEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    private String numeroCB;

    private String typeCarte;

    private Date echeance;

    private String codeCrypto;
}
