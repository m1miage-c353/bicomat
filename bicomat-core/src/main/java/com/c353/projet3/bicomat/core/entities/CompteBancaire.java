package com.c353.projet3.bicomat.core.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "comptebancaire")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CompteBancaire extends AbstractEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    @Column
    private String numeroCompte;

    @Column
    private String typeCompte;

    @Column
    private boolean decouvert;

    @Column
    private double tauxRenumeration;

    @ManyToOne
    @JoinColumn( name = "id_banque" )
    private Banque  banque;
}
