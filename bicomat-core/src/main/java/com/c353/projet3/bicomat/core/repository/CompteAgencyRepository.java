package com.c353.projet3.bicomat.core.repository;

import com.c353.projet3.bicomat.core.entities.CompteAgency;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompteAgencyRepository extends JpaRepository<CompteAgency,String> {
}
