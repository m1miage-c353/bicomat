package com.c353.projet3.bicomat.core.repository;

import com.c353.projet3.bicomat.core.entities.CarteBancaire;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarteBancaireRepository extends JpaRepository<CarteBancaire,String> {
}
