package com.c353.projet3.ebank.api.dto;

import com.c353.projet3.bicomat.models.BanqueDto;
import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;


@Data
@Builder
@AllArgsConstructor(onConstructor_ = {@JsonCreator(mode = JsonCreator.Mode.PROPERTIES)})
public class CreationBanqueRequest {
    private BanqueDto banqueDto;
}
