package com.c353.projet3.ebank.api.service;

import com.c353.projet3.bicomat.models.BanqueDto;
import com.c353.projet3.ebank.api.dto.CreationBanqueRequest;
import com.c353.projet3.ebank.api.dto.CreationBanqueResponse;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class BanqueService {
    RestTemplate restTemplate = new RestTemplate();

    public CreationBanqueResponse creerBanque(CreationBanqueRequest request){

        BanqueDto banque = restTemplate.postForObject("http://localhost:8888/core-metier/banques/save",request,BanqueDto.class);
        return CreationBanqueResponse.builder()
                        .banqueDto(banque)
                        .status(true)
                        .build();
    }
}
